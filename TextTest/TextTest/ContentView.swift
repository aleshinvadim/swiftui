//
//  ContentView.swift
//  TextTest
//
//  Created by aleshinvadim on 26.07.2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HStack(alignment: .center, spacing:100) {
        
            Text("ololo, world! AHAHAHAHA")
                .kerning(5)
                .lineLimit(nil)
                .font(.largeTitle)
                .truncationMode(.middle)
                .multilineTextAlignment(.center)
                .background(Color(#colorLiteral(red: 0.9650219083, green: 0.5626589656, blue: 0.9573044181, alpha: 1)))
            .padding(20)
            .background(Color.red)

        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
