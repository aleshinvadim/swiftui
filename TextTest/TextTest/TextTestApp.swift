//
//  TextTestApp.swift
//  TextTest
//
//  Created by aleshinvadim on 26.07.2022.
//

import SwiftUI

@main
struct TextTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
