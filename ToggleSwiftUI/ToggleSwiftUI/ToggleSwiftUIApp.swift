//
//  ToggleSwiftUIApp.swift
//  ToggleSwiftUI
//
//  Created by Trin Paprika on 8/26/22.
//

import SwiftUI

@main
struct ToggleSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
