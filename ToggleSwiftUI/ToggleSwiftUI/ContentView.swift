//
//  ContentView.swift
//  ToggleSwiftUI
//
//  Created by Trin Paprika on 8/26/22.
//

import SwiftUI

struct ContentView: View {
    @State var isOnToggle = false
    var body: some View {
        Text("Hello, world!")
         
        Toggle(isOn: $isOnToggle, label: {Text("Show options")})
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
