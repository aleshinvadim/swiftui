//
//  ContentView.swift
//  Alerts
//
//  Created by Trin Paprika on 7/29/22.
//

import SwiftUI

struct ContentView: View {
    @State var isError = false //будет хранить какое-то состояние
    
    
    var body: some View {
        Button(action: {
                    self.isError = true
                },
                label: {
                    Text("Enter")
        })
//            .alert(isPresented: $isError,
//                     content: {Alert(title: Text("Loading"),
//                                   message: Text("OLOLO?"),
//                                     primaryButton: .destructive(Text("YES"), action: {print ("you choosed YES")} ),
//                           secondaryButton:  .cancel())
//
//        })
            .actionSheet(isPresented: $isError, content: { ActionSheet(title: Text("Loading"), message: Text("Do you want to load image?"), buttons: [.destructive(Text("Load"), action: { print("image loaded")}), .cancel()])
                })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
