//
//  AlertsApp.swift
//  Alerts
//
//  Created by Trin Paprika on 7/29/22.
//

import SwiftUI

@main
struct AlertsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
