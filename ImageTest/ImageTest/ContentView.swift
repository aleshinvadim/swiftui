//
//  ContentView.swift
//  ImageTest
//
//  Created by Admin on 08.08.2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack(spacing:200) {
            Image(systemName: "cloud.sun.fill")
                .font(.largeTitle)
                .padding(20)
                .background(Color.black)
                .foregroundColor(.red)
                .clipShape(Capsule())
            ZStack{
                Text("Hello, world!")
                    .foregroundColor(.white)
                    .font(.largeTitle)
                    .padding()
                    .background(Image("180sx")
                        .resizable()
                        .frame(
                            width: 300, height: 300))
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
