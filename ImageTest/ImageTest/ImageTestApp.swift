//
//  ImageTestApp.swift
//  ImageTest
//
//  Created by Admin on 08.08.2022.
//

import SwiftUI

@main
struct ImageTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()

        }
    }
}
